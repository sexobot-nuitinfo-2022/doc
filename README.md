
# Docker defi 

on a joué ce defi sur l'application qu ' on a developé dans un autre defis de developement pendant la nuit .

on a creé 3 image docker pour notre projet qui est dedié à deveploper un bot discord intercative a pour but de : 

<ol>
<li>Sensibiliser les gens à l'importance d'utiliser des préservatifs lors des rapports sexuels.</li>
<li>Fournir des informations sur les symptômes des MST, y compris leur localisation et ce qu'il faut faire si vous pensez en être atteint.</li>
<li>Évaluer l'impact du manque de sensibilisation sur la santé des personnes.</li>
</ol>



## Lien de lapplication

http://137.184.76.192:8080

## notre architecture

<img src="./images/arch.png" style="margin:auto">

# executé le system


### comment buildé 

<img src="./images/script%20to%20build.png" style="margin:auto">

### script de deployement  

<img src="./images/to%20deploy.png" style="margin:auto">


## pull images de docker hub

#### sexobot-api

```bash 
docker pull hatembt/sexobot-api:latest
```

#### sexobot

```bash 
docker pull hatembt/sexobot-bot:latest
```

#### sexobot web client 

```bash 
docker pull hatembt/sexoblog-web-client:latest
```

### travail realisé 

#### portainer dashboard

<img src="./images/projects_repos.jpeg" style="margin:auto">

#### portainer dashboard

<img src="./images/portainer.jpeg" style="margin:auto">

#### build console 

<img src="./images/buildconsole.jpeg" style="margin:auto">

#### ports exposé

<img src="./images/portsexpose.jpeg" style="margin:auto">

#### api build 

<img src="./images/api_build.jpeg" style="margin:auto">

#### web app deployé 

<img src="./images/web_app.jpeg" style="margin:auto">
